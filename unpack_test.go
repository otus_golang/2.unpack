package unpack

import "testing"

func TestUnpack(t *testing.T) {
	string := "a4bc2d5e"
	want := "aaaabccddddde"
	if get := unpack(string); want != get {
		t.Errorf("want: %s | done: %s", want, get)
	}
	string = "abcd"
	want = "abcd"
	if get := unpack(string); want != get {
		t.Errorf("want: %s | done: %s", want, get)
	}
	string = "45"
	want = ""
	if get := unpack(string); want != get {
		t.Errorf("want: %s | done: %s", want, get)
	}
}