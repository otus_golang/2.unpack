package unpack

import (
	"unicode"
)

func unpack(packedString string) string {
	result := ""
	if len(packedString) == 0 {
		return result
	}

	var prevRune rune

	for _, runeValue := range packedString {
		if !unicode.IsDigit(runeValue) {
			result += string(runeValue)
		} else {
			if int(prevRune) == 0 {
				return ""
			}
			for i := 0; i < int(runeValue - '1'); i++ {
				result += string(prevRune)
			}
		}
		prevRune = runeValue
	}
	return result
}
